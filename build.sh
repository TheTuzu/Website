#!/bin/sh
include_syntax='@include'

# Setup build directory
rm -r ./out
mkdir -p out
cd ./out
cp -r $(find ../ -maxdepth 1 ! -path '*.git*' ! -path '*out*' ! -path "*build.sh" ! -path '../' | xargs) ./

for i in $(find -iname "*.html" ! -path "./out*"); do
  include_current=0
  echo $i
  if grep "@include " $i; then
    include_total_count=$(grep "@include " $i | wc -l)
    while [ $include_total_count -ne $include_current ]; do
      include_current=$((include_current + 1))
      include_file=$(grep "@include " $i | awk '{print $2}' | sed -n "1p")
      sed -i "s^@include $include_file^$(cat $include_file | xargs)^" $i
    done
  fi
done
